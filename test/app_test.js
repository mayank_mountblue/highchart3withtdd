
var chai =require("chai").should();
var graph_api= require("../extra-run-conceded-by-each-team.js");
//var assert = require('assert');

  describe('extra run given by the bowler', function() {
    it('should return all the match_id ofr particular season', function(){
     var data= [
       { id: '99',
      season: '2008',
      team1: 'Kolkata Knight Riders',
      team2: 'Mumbai Indians' },
      { id: '99',
      season: '2016',
      team1: 'Kolkata Knight Riders',
      team2: 'Mumbai Indians'
     }
    
    ]
      
      var season ="2008";
      var expected =[99];
     
     var result=graph_api.list_of_match_id_of_season(data,season);
    
    result.should.deep.equal(expected);
    });

    it("extra run given by each team in 2016",function(){
     var match =[{ id: '100',
     season: '2008',
     city: 'Delhi',
     }];
     var delivery =[{ match_id: '100',
     inning: '1',
     bowling_team: 'Royal Challengers Bangalore',
     over: '16',
     ball: '4',
     extra_runs: '0' }];

     var result = graph_api.get_extra_run_given_by_each_team(delivery,match,"2008");
    
 var expected =[ ["Royal Challengers Bangalore", 0 ]];
  result.should.deep.equal(expected);

    });
  });
