var express = require("express");
var router = express.Router();
var app = express();
var bp = require("body-parser");
var path = require("path");
var fs = require("fs");
var csvjson = require('csvjson');
var $ = require('jquery');



var match = fs.readFileSync(path.join(__dirname, './data/matches.csv'), {
	encoding: 'utf8'
});
var delivery = fs.readFileSync(path.join(__dirname, './data/deliveries.csv'), {
	encoding: 'utf8'
});
var jsondelivery = csvjson.toSchemaObject(delivery);
var jsonmatches = csvjson.toObject(match);
var get_extra_run_given_by_each_team = function (delivery, match, season) {
		var match_id = list_of_match_id_of_season(match, season);


		var arr = {};
		var extra_runs_per_team_on_season = delivery.map(function (deliveries) {

				if (match_id.includes(parseInt(deliveries.match_id))) {
					var team = deliveries.bowling_team;
				
					if(arr[team]){
						arr[team] += parseInt(deliveries.extra_runs);	
					} else{
						console.log(deliveries.extra_run);
						arr[team] = parseInt(deliveries.extra_runs);
					}
 				}
		});

			var extra_runs_per_team = [];

			Object.keys(arr).forEach(function (key) {
				var team_and_extra_run = [];
				team_and_extra_run[0] = key;
				team_and_extra_run[1] = arr[key];
				extra_runs_per_team.push(team_and_extra_run);

			});

			return extra_runs_per_team;
		}


		list_of_match_id_of_season = function (matches, season) {
			var match_id_of_season = [];

			for (var i in matches) {
				var obj;
				if (matches[i].season == season) {

					obj = parseInt(matches[i].id);
					match_id_of_season.push(obj);
				}


			}
			return match_id_of_season;
		}

		var data = get_extra_run_given_by_each_team(jsondelivery, jsonmatches, "2016");
		console.log(data);
		router.get("/", function (req, res) {
			res.send(data);
		});




		module.exports = {
			get_extra_run_given_by_each_team: get_extra_run_given_by_each_team,
			list_of_match_id_of_season: list_of_match_id_of_season,
			router: router
		}